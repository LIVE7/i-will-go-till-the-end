﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDataBase : MonoBehaviour {
	
	public List<Item> items = new List<Item>();
	public List<Mix> mix = new List<Mix> ();
	public GameObject[] RealItem;

	void Start () 
	{
		
		items.Add (new Item ("풀", 0, "평범한 풀이다 ", 10, Item.ItemType.Other,RealItem[0]));
		items.Add (new Item ("고기", 1, "평범한 고기이다 ", 30, Item.ItemType.Other,RealItem[1]));
		items.Add (new Item ("당근", 2, "어딘가 쓸모가 있을 지도???", 100, Item.ItemType.Other,RealItem[2]));
		items.Add (new Item ("마취주사",3, "마취총의 총알이다.", 100, Item.ItemType.Other,RealItem[3]));
		items.Add (new Item ("벽",4, "단단한 국내산 벽이다", 100, Item.ItemType.Building,RealItem[4]));


		mix.Add (new Mix( 0, 1, 2));

	}
	

}
