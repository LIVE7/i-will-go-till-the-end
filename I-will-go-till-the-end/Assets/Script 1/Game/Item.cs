﻿using UnityEngine;
using System.Collections;

public class Item{

	public string itemName;
	public int itemID;
	public ItemType itemtype;
	public string itemDesc;
	public Sprite itemIcon;
	public int itemValue;
	public GameObject itemMode1;
	public GameObject RealItem;

	public enum ItemType
	{
		Weapon,
		Building,
		Other
	}

	public Item(string name, int id, string desc,int value, ItemType type, GameObject item)
	{
		itemName = name;
		itemID = id;
		itemDesc = desc;
		itemValue = value;
		itemtype = type;
		itemIcon = Resources.Load<Sprite> ("" + name);
		RealItem = item;
	}

	public Item()
	{

	}
}
