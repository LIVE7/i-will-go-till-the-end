﻿using UnityEngine;
using System.Collections;

public class DropItem : MonoBehaviour {

	public Inventory inventory;
	public RectTransform invenRT;
	public ItemDataBase itemDB;
	public Player player;
	public int main;
	void Start () 
	{

	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			if (inventory.draggingItem) 
			{
				Vector2 MousePos = Camera.main.ScreenToViewportPoint (Input.mousePosition);


				if (!(MousePos.x > invenRT.anchorMin.x && MousePos.x < invenRT.anchorMax.x && MousePos.y > invenRT.anchorMin.y && MousePos.y < invenRT.anchorMax.y) && inventory.buildCollider.CanBuild) 
				{

					//Vector2 WorldMousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					GameObject obj = Instantiate (inventory.draggedItem.RealItem, inventory.buildPos, transform.rotation) as GameObject;
					obj.GetComponent<Build> ().player = player;

					if(obj.transform.position.x > player.transform.position.x)
						player.transform.eulerAngles = new Vector3 (0, 0, 0);
					else
						player.transform.eulerAngles = new Vector3 (0, 180, 0);

					player.state = Player.State.Build;
					Destroy (inventory.BuildPreview);
					inventory.RemoveItem(inventory.draggedItem.itemID, inventory.draggingCount);
					inventory.CloseDraggedItem ();
					if (inventory.draggingCount > 1)
					{
						inventory.AddItem(inventory.draggedItem.itemID, inventory.draggingCount-1);

					}

				}

			}
		}
	}
}
