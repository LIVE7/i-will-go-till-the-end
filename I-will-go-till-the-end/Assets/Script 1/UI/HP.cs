﻿using UnityEngine;
using System.Collections;

public class HP : MonoBehaviour {
	public GameObject[] life;
	private int live;
	// Use this for initialization
	void Start () {
		live = 3;
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "enemy")
		{
			if(live>0)
			{
				life[3-(int)live].SetActive (false);
				live-=1;
			}
			if(live==0){
				Destroy(gameObject);
			}
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
