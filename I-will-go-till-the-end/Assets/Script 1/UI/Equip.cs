﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Equip : MonoBehaviour
{
	public int[] slotSize = new int[2]{ 5, 6 };

	public List<GameObject> Slots = new List<GameObject> ();
	public List<Item> Items = new List<Item> ();
	public GameObject slots;
	public float[] slotPos_X = new float[2];
	public float[] slotPos_Y = new float[2];

	public GameObject DataBase;
	ItemDataBase database;

	public GameObject draggedItemGameObject;
	public GameObject tooltip;
	public bool draggingItem = false;
	public Item draggedItem;
	public int draggingCount;
	public int indexOfDraggedItem;

	public int CountItem(Item item)
	{
		int count = 0;
		for(int i = 0 ; i < Items.Count ; i++)
		{
			if(Items[i].itemName == item.itemName)
			{
				count += Slots[i].GetComponent<Slot>().ItemCount;
			}
		}
		return count;
	}


	void Update()
	{


		if(draggingItem == true)
		{
			float AnchorGap_X = draggedItemGameObject.GetComponent<RectTransform> ().anchorMax.x - draggedItemGameObject.GetComponent<RectTransform> ().anchorMin.x;
			float AnchorGap_Y = draggedItemGameObject.GetComponent<RectTransform> ().anchorMax.y - draggedItemGameObject.GetComponent<RectTransform> ().anchorMin.y;
			draggedItemGameObject.GetComponent<RectTransform> ().anchorMin = new Vector2 (Input.mousePosition.x / Screen.width + 0.001f,
				Input.mousePosition.y / Screen.height - AnchorGap_Y - 0.001f);
			draggedItemGameObject.GetComponent<RectTransform> ().anchorMax = new Vector2 (Input.mousePosition.x / Screen.width + AnchorGap_X + 0.001f,
				Input.mousePosition.y / Screen.height- 0.001f);

			draggedItemGameObject.GetComponent<RectTransform> ().offsetMin = new Vector2 (0, 0);
			draggedItemGameObject.GetComponent<RectTransform> ().offsetMax = new Vector2 (0, 0);
		}
	}
	public void ShowDraggedItem(Item item, int slotNum, int cnt)
	{
		indexOfDraggedItem = slotNum;
		draggedItemGameObject.SetActive (true);
		draggedItem = item;
		draggingItem = true;
		draggingCount = cnt;
		draggedItemGameObject.GetComponent<Image> ().sprite = item.itemIcon;
	}

	public void CloseDraggedItem()
	{
		draggingItem = false;
		draggedItemGameObject.SetActive (false);

	}
	public void CloseTooltip()
	{
		tooltip.SetActive (false);
	}



	public void ShowTooltip(Vector3 toolPosition, Item item)
	{
		float AnchorGap_X = tooltip.GetComponent<RectTransform> ().anchorMax.x - tooltip.GetComponent<RectTransform> ().anchorMin.x;
		float AnchorGap_Y = tooltip.GetComponent<RectTransform> ().anchorMax.y - tooltip.GetComponent<RectTransform> ().anchorMin.y;
		tooltip.GetComponent<RectTransform> ().anchorMin = new Vector2 (toolPosition.x / Screen.width + 0.001f,
			toolPosition.y / Screen.height - AnchorGap_Y - 0.001f);
		tooltip.GetComponent<RectTransform> ().anchorMax = new Vector2 (toolPosition.x / Screen.width + AnchorGap_X + 0.001f,
			toolPosition.y / Screen.height - 0.001f);

		tooltip.GetComponent<RectTransform> ().offsetMin = new Vector2 (0, 0);
		tooltip.GetComponent<RectTransform> ().offsetMax = new Vector2 (0, 0);


		tooltip.transform.GetChild (0).GetComponent<Text> ().text = item.itemName;
		tooltip.transform.GetChild (1).GetComponent<Text> ().text = item.itemDesc;

		tooltip.SetActive (true);
	}

	// Use this for initialization


	void Start ()
	{



		//AddItem (0, 2);


		int Slotamount = 0;
		database = DataBase.GetComponent<ItemDataBase> ();
		float AnchorGap_X = slots.GetComponent<RectTransform> ().anchorMax.x - slots.GetComponent<RectTransform> ().anchorMin.x;
		float AnchorGap_Y = slots.GetComponent<RectTransform> ().anchorMax.y - slots.GetComponent<RectTransform> ().anchorMin.y;

		for(int i = 0 ;i < slotSize[1] ; i++)
		{
			for(int k = 0; k < slotSize[0] ; k++)
			{
				GameObject slot = (GameObject)Instantiate(slots);
				slot.GetComponent<Slot>().slotNum = Slotamount;
				Slots.Add(slot);
				Items.Add(new Item());
				slot.transform.SetParent(gameObject.transform);
				slot.GetComponent<RectTransform>().anchorMin = new Vector2(slotPos_X[0]+((slotPos_X[1] -slotPos_X[0])/(slotSize[0]-1) * k ),
					slotPos_Y[0]+((slotPos_Y[1] -slotPos_Y[0])/(slotSize[1]-1) * i ));
				slot.GetComponent<RectTransform>().anchorMax = new Vector2(slot.GetComponent<RectTransform>().anchorMin.x + AnchorGap_X,
					slot.GetComponent<RectTransform>().anchorMin.y + AnchorGap_Y );
				slot.GetComponent<RectTransform>().offsetMax = new Vector2(0,0);
				slot.GetComponent<RectTransform>().offsetMin = new Vector2(0,0);
				slot.name = "slot"+(i+1)+"."+(k+1);

				Slotamount++;
			}
		}


		AddItem (3, 2);
		AddItem (3, 4);

	}

	void addItemAtEmptySlot(Item item , int cnt)
	{
		for(int i = 0 ; i < Items.Count ; i++)
		{
			if(Items[i].itemName == null)
			{
				Items[i] = item;

				Slots[i].GetComponent<Slot>().ItemCount = cnt;
				break;
			}
		}
	}

	void addItemAtNotEmptySlot(Item item, int cnt)
	{
		for(int i = 0 ; i < Items.Count ; i++)
		{
			if(Items[i].itemName == item.itemName)
			{
				Slots[i].GetComponent<Slot>().ItemCount += cnt;
				break;
			}
		}
	}

	public void AddItem(int id, int cnt)
	{

		for(int i = 0; i < database.items.Count ; i++)
		{


			if(database.items[i].itemID == id)
			{

				Item item = database.items[i];

				if(item.itemtype == Item.ItemType.Weapon)
				{
					addItemAtEmptySlot(item, cnt);
				}
				else
				{
					bool isItemExsit = false;
					for(int g = 0 ; g < Items.Count ; g++)
					{
						if(Items[g].itemName == item.itemName)
						{
							isItemExsit = true;
							break;
						}
					}

					if(isItemExsit == true)
						addItemAtNotEmptySlot(item, cnt);
					else
						addItemAtEmptySlot(item, cnt);
				}


				break;
			}
		}
	}
	void removeItemAtSlot(Item item, int cnt)
	{
		for(int i = 0 ; i < Items.Count ; i++)
		{
			if(Items[i].itemName == item.itemName)
			{
				Slots[i].GetComponent<Slot>().ItemCount -= cnt;

				if(Slots[i].GetComponent<Slot>().ItemCount <= 0)
				{
					Items[i] = new Item();
				}
				break;
			}
		}
	}
	public void RemoveItem(int id, int cnt)
	{
		for(int i = 0; i < database.items.Count ; i++)
		{
			if(database.items[i].itemID == id)
			{
				Item item = database.items[i];
				removeItemAtSlot(item,cnt);
				break;
			}
		}
	}








}
