﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot :MonoBehaviour
{
	public Item item;
	Image itemImage;
	public int slotNum;
	Inventory inventroy;
	ItemDataBase database;
	public int ItemCount;
	Player player;
	public void asdf(){
		Debug.Log("야호");
	}

	// Use this for initialization
	void Start () 
	{

		inventroy = transform.parent.GetComponent<Inventory> ();
		player = inventroy.playerComponent;
		database = inventroy.database;
		itemImage = gameObject.transform.GetChild (0).GetComponent<Image> ();
	}


	int JudgeExistMix()
	{
		for (int i = 0; i < database.mix.Count; i++) 
		{
			if (database.mix [i].itemNum1 == item.itemID) 
			{
				if (database.mix [i].itemNum2 == inventroy.draggedItem.itemID) 
				{
					return i;
				}
			} 
			else if (database.mix [i].itemNum1 == inventroy.draggedItem.itemID) 
			{
				if (database.mix [i].itemNum2 == item.itemID) 
				{
					return i;
				}
			}
		}
			
		return -1;
	}


	// Update is called once per frame
	void Update () 
	{
		if(inventroy.Items[slotNum].itemName != null )
		{

			item = inventroy.Items[slotNum];
			itemImage.enabled = true;
			itemImage.sprite = inventroy.Items[slotNum].itemIcon;

			if(inventroy.Items[slotNum].itemtype == Item.ItemType.Other || inventroy.Items[slotNum].itemtype == Item.ItemType.Building)
			{
				transform.FindChild("ItemCount").GetComponent<Text>().text = "" + ItemCount;
			}
			else
			{
				transform.FindChild("ItemCount").GetComponent<Text>().text = "";
			}
		}
		else
		{
			itemImage.enabled = false;
			transform.FindChild("ItemCount").GetComponent<Text>().text = "";
		}
	}

	void PointerEnter()
	{
		if(inventroy.Items[slotNum].itemName != null && inventroy.draggingItem == false)
		{
			inventroy.ShowTooltip(Input.mousePosition , item);
		}
	}

	void PointerDown()
	{
		if (player.state != Player.State.Build) 
		{
			if (inventroy.Items [slotNum].itemName == null && inventroy.draggingItem == true) 
			{
				inventroy.Items [slotNum] = inventroy.draggedItem;
				inventroy.Slots [slotNum].GetComponent<Slot> ().ItemCount = inventroy.draggingCount;
				inventroy.CloseDraggedItem ();
			} 
			else if (inventroy.draggingItem && inventroy.Items [slotNum].itemName != null) 
			{

				if (JudgeExistMix () == -1) 
				{
					inventroy.Items [inventroy.indexOfDraggedItem] = inventroy.Items [slotNum];
					inventroy.Slots [inventroy.indexOfDraggedItem].GetComponent<Slot> ().ItemCount = inventroy.Slots [slotNum].GetComponent<Slot> ().ItemCount;
					inventroy.Slots [slotNum].GetComponent<Slot> ().ItemCount = inventroy.draggingCount;
					inventroy.Items [slotNum] = inventroy.draggedItem;
					inventroy.CloseDraggedItem ();
				}
				else 
				{
					if (inventroy.draggingCount > 1)
					{
						inventroy.AddItem (inventroy.indexOfDraggedItem, inventroy.draggingCount - 1);
					} 

					inventroy.RemoveItem (item.itemID, 1);
					inventroy.AddItem (database.mix [JudgeExistMix ()].resultItem, 1);


					inventroy.CloseDraggedItem ();
				}


			}



		}
	}

	void PointerExit()
	{
		if(inventroy.Items[slotNum].itemName != null)
		{
			inventroy.CloseTooltip();
		}
	}
	void Drag()
	{
		if(inventroy.Items[slotNum].itemName != null && player.state != Player.State.Build)
		{

			inventroy.ShowDraggedItem(item, slotNum, ItemCount);
			inventroy.Items[slotNum] = new Item();
			ItemCount = 0;
			inventroy.CloseTooltip();
		}
	}

}
