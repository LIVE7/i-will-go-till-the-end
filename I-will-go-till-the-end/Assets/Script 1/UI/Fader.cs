﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Fader : MonoBehaviour {

	Image fader;
	public bool isFadein = false; 
	public float fadeTime = 0.5f;
	/*
	IEnumerator Fade()
	{
		isFadein = true;
		fader.color = new Color(fader.color.r,
		                        fader.color.g,
		                        fader.color.b,
		                        0);
		yield return new WaitForSeconds (0.3f);

		fader.color = new Color(fader.color.r,
		                        fader.color.g,
		                        fader.color.b,
		                        1);
		isFadein = false;
	}
	*/
	public void StartFade()
	{
		StartCoroutine("Fade");
	}
	void Start ()
	{
		fader = GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isFadein == true && fader.color.a < 1)
		{
			fader.color = new Color(fader.color.r,
			                        fader.color.g,
			                        fader.color.b,
									fader.color.a + (Time.deltaTime / fadeTime));
		}
		else if(isFadein == false && fader.color.a > 0)
		{
			fader.color = new Color(fader.color.r,
			                        fader.color.g,
			                        fader.color.b,
									fader.color.a - (Time.deltaTime / fadeTime));
		}




		if (fader.color.a < 0)
			gameObject.SetActive (false);
	}
}
