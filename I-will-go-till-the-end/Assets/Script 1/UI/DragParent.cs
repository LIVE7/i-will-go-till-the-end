﻿using UnityEngine;
using System.Collections;

public class DragParent: MonoBehaviour {

	Vector2 DragStart_Mouse;
	Vector2 DragStart_Anchor_Max;
	float Gap_Anchor_X;
	float Gap_Anchor_Y;
	Transform Parent;
	void Start()
	{
		Parent = transform.parent;
	}
	void Press()
	{
		DragStart_Mouse = new Vector2 (Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
		DragStart_Anchor_Max = Parent.GetComponent<RectTransform> ().anchorMax;
		Gap_Anchor_X = Parent.GetComponent<RectTransform> ().anchorMax.x - Parent.GetComponent<RectTransform> ().anchorMin.x;
		Gap_Anchor_Y = Parent.GetComponent<RectTransform> ().anchorMax.y - Parent.GetComponent<RectTransform> ().anchorMin.y;
	}
	
	void Drag () 
	{
		
		Vector2 Draging_Mouse = new Vector2 (Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
		Vector2 NowPosition;
		
		NowPosition = DragStart_Anchor_Max + (Draging_Mouse - DragStart_Mouse);
		
		
		if (NowPosition.x > 1)
			NowPosition = new Vector2 (1, NowPosition.y);
		else if(NowPosition.x < 0 + Gap_Anchor_X)
			NowPosition = new Vector2 ( 0 + Gap_Anchor_X, NowPosition.y);
		if (NowPosition.y > 1)
			NowPosition = new Vector2 (NowPosition.x, 1);
		else if(NowPosition.y < 0 + Gap_Anchor_Y)
			NowPosition = new Vector2 ( NowPosition.x, 0 + Gap_Anchor_Y);
		
		
		Parent.GetComponent<RectTransform> ().anchorMax = NowPosition;
		Parent.GetComponent<RectTransform> ().anchorMin = NowPosition - new Vector2 (Gap_Anchor_X, Gap_Anchor_Y);
		
	}
}
