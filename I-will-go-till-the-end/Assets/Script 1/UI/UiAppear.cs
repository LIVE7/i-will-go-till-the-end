﻿using UnityEngine;
using System.Collections;

public class UiAppear : MonoBehaviour {

	float[] anchor_X = new float[2];
	float[] anchor_Y = new float[2];
	public bool isAppear = false;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(isAppear == false && GetComponent<RectTransform>().anchorMax.x != 0)
		{
			anchor_X[0] = GetComponent<RectTransform>().anchorMin.x;
			anchor_X[1] = GetComponent<RectTransform>().anchorMax.x;
			anchor_Y[0] = GetComponent<RectTransform>().anchorMin.y;
			anchor_Y[1] = GetComponent<RectTransform>().anchorMax.y;
			
			GetComponent<RectTransform>().anchorMax = new Vector2(0,0);
			GetComponent<RectTransform>().anchorMin = new Vector2(0,0);
			
		}
		else if(isAppear == true && GetComponent<RectTransform>().anchorMax.x == 0)
		{
			GetComponent<RectTransform>().anchorMin = new Vector2(anchor_X[0], anchor_Y[0]);
			GetComponent<RectTransform>().anchorMax = new Vector2(anchor_X[1], anchor_Y[1]);
			
			GetComponent<RectTransform> ().offsetMin = new Vector2 (0, 0);
			GetComponent<RectTransform> ().offsetMax = new Vector2 (0, 0);
		}
	}
}
