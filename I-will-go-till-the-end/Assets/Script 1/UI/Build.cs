﻿using UnityEngine;
using System.Collections;

public class Build : MonoBehaviour {

	public float buildSpeed = 3;
	public float progressBarPos = 2;
	public GameObject progressBar;
	public Player player;
	float percent = 0;
	SpriteRenderer SR;
	ProgressBar PB;
	void Start () 
	{
		GameObject p = Instantiate (progressBar, transform.position + new Vector3 (0, progressBarPos, 0), progressBar.transform.rotation) as GameObject;
		PB = p.GetComponent<ProgressBar>();
		SR = GetComponent<SpriteRenderer> ();
		GetComponent<ItemCollider> ().enabled = false;
		GetComponent<Collider2D> ().isTrigger = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		percent += Time.deltaTime * 100 / buildSpeed;
		PB.percent = percent;

		SR.color = new Color (1, 1, 1,0.5f + 0.5f / 100.0f * percent);
		if (percent > 100 && PB != null)
		{
			player.state = Player.State.Idle;
			GetComponent<Collider2D> ().isTrigger = false;
			Destroy (PB.gameObject);
		}
	}
}
