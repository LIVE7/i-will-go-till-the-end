﻿using UnityEngine;
using System.Collections;

public class JumpButton : MonoBehaviour {

	public Rigidbody2D playerRigidBody;
	public Player player;
	public Animator anim;
	void OnClick ()
	{
		if (player.Lands.Count > 0 && player.state != Player.State.Build) 
		{
			playerRigidBody.AddForce (Vector2.up * 300);
			anim.SetBool ("isJump", true);
			player.PlayJumpSound ();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
