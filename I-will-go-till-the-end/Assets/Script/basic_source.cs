﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class basic_source : MonoBehaviour {

	public static int A;
	public static int B;
	public static int C;
	Text text;
	// Use this for initialization
	void Awake () {
		text = GetComponent<Text> ();
		A = 0;
		B = 0;
		C = 0;
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "광물: " + A + " 나무: " + B + " 쇠붙이:" + C;
	}
}
