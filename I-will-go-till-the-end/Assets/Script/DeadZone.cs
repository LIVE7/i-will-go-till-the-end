﻿using UnityEngine;
using System.Collections;

public class DeadZone : MonoBehaviour {

	public GameObject gameOverPanel;
	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.tag == "Player") 
		{
			gameOverPanel.SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
