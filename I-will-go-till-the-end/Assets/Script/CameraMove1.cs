﻿using UnityEngine;
using System.Collections;

public class CameraMove1 : MonoBehaviour {

	public GameObject player;

	public float[] limitX = new float[2];
	public float[] limitY = new float[2];

	Vector3 goalPos;
	Vector2 screen;
	void Start () 
	{
		screen = Camera.main.ViewportToWorldPoint (new Vector2 (1f, 1f)) - Camera.main.ViewportToWorldPoint (new Vector2 (0.5f, 0.5f));
		goalPos = new Vector3(player.transform.position.x , player.transform.position.y , -10);
	}
	
	// Update is called once per frame
	void Update ()
	{

		goalPos = new Vector3(player.transform.position.x , player.transform.position.y , -10);

		if (goalPos.x - screen.x < limitX [0]) 
		{
			goalPos = new Vector3 (limitX [0] + screen.x,goalPos.y,-10);
		}
		else if(goalPos.x + screen.x > limitX [1]) 
		{
			goalPos = new Vector3 (limitX [1] - screen.x,goalPos.y,-10);
		}


		if (goalPos.y - screen.y < limitY [0]) 
		{
			goalPos = new Vector3 (goalPos.x , limitY[0] + screen.y,-10);
		}
		else if(goalPos.y + screen.y > limitY [1]) 
		{
			goalPos = new Vector3 (goalPos.x , limitY[1] - screen.y,-10);
		}




		transform.position = Vector3.Lerp (transform.position, goalPos, Time.deltaTime * 2);

	}
}
