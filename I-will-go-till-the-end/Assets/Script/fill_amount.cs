﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class fill_amount : MonoBehaviour {
	public Image roof;
	public float time;
	public GameObject gameOverPanel;
	void Start () {
		roof.fillAmount = 1;
	}
	
	// Update is called once per frame
	void Update () {
		


		if (roof.fillAmount <= 0) 
		{
			gameOverPanel.SetActive (true);
		} 
		else 
		{
			roof.fillAmount -=Time.deltaTime/time;
		}
	}
}
