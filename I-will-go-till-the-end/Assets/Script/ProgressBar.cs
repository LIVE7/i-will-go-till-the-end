﻿using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {

	public Transform bar;
	public float percent = 0;

	float maxwidth;
	void Start () 
	{
		bar = transform.GetChild (0);
		maxwidth = bar.localScale.x;
	}
	
	// Update is called once per frame
	void Update () 
	{
		bar.localScale = new Vector2 (maxwidth / 100 * percent, bar.localScale.y);
	}
}
