﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public Animator anim;
	public Slider moveSlider;
	public float speed;
	public State state;

	public AudioClip[] AC;
	AudioSource AS;
	public List<GameObject> Lands = new List<GameObject>();


	public void PlayJumpSound()
	{
		AS.clip = AC [0];
		AS.Play ();
	}



	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Land" && !Lands.Contains (coll.gameObject)) 
		{
			Lands.Add (coll.gameObject);

			anim.SetBool ("isJump", false);
		}
	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Land" && Lands.Contains (coll.gameObject)) 
		{
			Lands.Remove (coll.gameObject);
		}
	}





	public enum State
	{
		Build,
		Idle,
		Walk

	}
	void Start () 
	{
		state = State.Idle;
		AS = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		



		if (state != State.Build)
		{
			AS.loop = false;


			anim.SetBool ("isBuild", false);

			transform.position = transform.position + new Vector3 (moveSlider.value * speed * Time.deltaTime, 0, 0);  


			anim.SetFloat ("speed", Mathf.Abs (moveSlider.value));

			if (moveSlider.value > 0) 
			{
				transform.eulerAngles = new Vector3 (0, 0, 0);

			} 
			else if (moveSlider.value < 0) 
			{
				transform.eulerAngles = new Vector3 (0, 180, 0);
			}
		} 
		else 
		{
			if (!AS.isPlaying)
			{
				AS.clip = AC [1];
				AS.loop = true;
				AS.Play ();
			}

			anim.SetBool ("isBuild", true);
		}
	}
}
