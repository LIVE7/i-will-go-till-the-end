﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartButton : MonoBehaviour {

	public Fader fader;
	Image faderColor;
	public float fadeTime = 0.5f;
	public bool isNewGame = false;
	bool isClicked = false;
	public AudioSource AS;
	void Start()
	{
		faderColor = fader.GetComponent<Image> ();
		DontDestroyOnLoad (AS.gameObject);
	}
	public void OnClick ()
	{
		
		fader.gameObject.SetActive (true);
		fader.isFadein = true;
		isClicked = true;
		AS.Play ();

	}
	
	// Update is called once per frame
	void Update ()
	{



		if (fader.gameObject.activeInHierarchy && isClicked)
		{
			if (faderColor.color.a > 0.99f) 
			{
				if (isNewGame)
				{
					PlayerPrefs.SetFloat ("PosX", 0);
					PlayerPrefs.SetFloat ("PosY", 0);

					for (int i = 0; i < 9; i++)
					{
						PlayerPrefs.SetInt ("ItemID"+i, 0);
						PlayerPrefs.SetInt ("ItemCount"+i, 0);
					}
					Application.LoadLevel ("1");
				} 
				else
				{
					Application.LoadLevel ("2");
				}
			}
		}
	}
}
