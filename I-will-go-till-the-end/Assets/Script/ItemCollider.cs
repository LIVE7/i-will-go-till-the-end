﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemCollider : MonoBehaviour {

	public List<GameObject> connected = new List<GameObject>();
	public bool CanBuild = false;
	void OnTriggerEnter2D (Collider2D coll) 
	{
		if (coll.tag == "building" || coll.tag == "Player")
		{
			if (!connected.Contains (coll.gameObject))
			{
				connected.Add (coll.gameObject);
			}
					
		}
	
	}


	void OnTriggerExit2D (Collider2D coll) 
	{
		if (coll.tag == "building" || coll.tag == "Player")
		{
			connected.Remove (coll.gameObject);
		}

	}

	
	// Update is called once per frame
	void Update ()
	{

	}
}
