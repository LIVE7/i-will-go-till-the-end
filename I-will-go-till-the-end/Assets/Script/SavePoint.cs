﻿using UnityEngine;
using System.Collections;

public class SavePoint : MonoBehaviour {

    public GameObject player;
	public Inventory inventory;
	float time = 0;
	void OnTriggerEnter2D (Collider2D coll) 
	{
		//print ("dsa");
		if (coll.gameObject.tag == "Player" && time > 0.5f)
		{
			print ("save");
			PlayerPrefs.SetFloat ("PosX", player.transform.position.x);
			PlayerPrefs.SetFloat ("PosY", player.transform.position.y);

			for (int i = 0; i < inventory.Slots.Count; i++)
			{
				Slot slot = inventory.Slots [i].GetComponent<Slot> ();
				if (slot.item != null) 
				{
					PlayerPrefs.SetInt ("ItemID" + i, inventory.Slots [i].GetComponent<Slot> ().item.itemID);
					PlayerPrefs.SetInt ("ItemCount" + i, inventory.Slots [i].GetComponent<Slot> ().ItemCount);
				} 
				else 
				{
					PlayerPrefs.SetInt ("ItemID" + i, -1);
					PlayerPrefs.SetInt ("ItemCount" + i, 0);
				}
			}
		}
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (time < 0.5f)
		{
			time += Time.deltaTime;
		}

	}
}
