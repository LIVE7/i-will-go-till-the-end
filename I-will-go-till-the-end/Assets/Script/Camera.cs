﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    GameObject player;

	void Start () {

        player = GameObject.FindWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void LateUpdate()
    {
        transform.position = new Vector3(player.transform.position.x, 0f, -10f);
    }
}
