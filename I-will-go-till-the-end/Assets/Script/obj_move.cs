﻿using UnityEngine;
using System.Collections;

public class obj_move : MonoBehaviour {


	public float time;
	public float speed;

	bool textup = true;

	void Start()
	{
		StartCoroutine("moveobj");
	}
	void Update () 
	{
		if (textup == true) 
		{
			transform.Translate (Vector2.up * speed * Time.deltaTime);
		} 
		else 
		{
			transform.Translate (-Vector2.up * speed * Time.deltaTime);
		}

	}
	IEnumerator moveobj()
	{
		textup = true;
		yield return new WaitForSeconds(time);
		textup = false;
		yield return new WaitForSeconds(time);

		StartCoroutine("moveobj");
	}
}