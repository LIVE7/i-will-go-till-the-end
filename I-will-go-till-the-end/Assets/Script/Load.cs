﻿using UnityEngine;
using System.Collections;

public class Load : MonoBehaviour {

	public GameObject player;
	public Inventory inventory;

	void LoadData () 
	{
		if(PlayerPrefs.GetFloat("PosX") != 0 || PlayerPrefs.GetFloat("PosY") != 0)
			player.transform.position = new Vector3 (PlayerPrefs.GetFloat("PosX"),PlayerPrefs.GetFloat("PosY"),0);

		for (int i = 0; i < inventory.Slots.Count; i++) 
		{
			int itemID = PlayerPrefs.GetInt ("ItemID" + i);
			int itemCount = PlayerPrefs.GetInt ("ItemCount" + i);
			if (itemID != -1 && itemCount != 0)
			{
				

				inventory.AddItem (itemID, itemCount);

			}

		}

	}
	
	// Update is called once per frame
	void Start () 
	{
		LoadData ();
	}



}
